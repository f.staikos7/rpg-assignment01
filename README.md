# RPG Assignment01



This is a game hero implementation in Java, it includes:


- Four hero classes: Mage, Ranger, Rogue and Warrior.
- An abstract class called Hero that encapsulates all the shared functionality (fields and methods) between the hero classes.
- A class called HeroAttribute that encapsulates the attributes of the hero: Strength, Dexterity and Intelligence.
- Two types of items: Weapons and Armor.
- An abstract class called Item that encapsulates all the shared functionality (fields and methods) between the two item types.
- An enumerator called Slot that represents the slot in which the item will be equipped.
- An enumerator called WeaponType that represents the type of weapon the hero is using.
- An enumerator called ArmorType that represents the type of armor the hero is using.
- A custom exception called InvalidItemException that is thrown when a hero tries to equip an item that is not valid for him.
- A custom exception called InvalidWeaponException that is thrown when a hero tries to equip a weapon that is not valid for him.
- A custom exception called InvalidArmorException that is thrown when a hero tries to equip an armor that is not valid for him.
- A method called display that returns a string representation of the hero.


## Usage

To use this code, you can create an instance of any of the four hero classes by passing the hero's name as an argument. Once you have a hero object, you can call the following methods:

- **levelUp()**: increases the hero's level by 1 and increases their attributes.
- **equip(Item item)**: equips the hero with an item.
- **damage()**: calculates the hero's damage based on their currently equipped weapon and attributes.
- **totalAttributes()**: calculates the hero's total attributes based on their levelling attributes and the attributes from the armor they have equipped.
- **display()**: returns a string representation of the hero's state.

## Example

```
// create a new Warrior named "John"
Hero warrior = new Warrior("John");

// create a new Weapon with the following state
Weapon weapon = new Weapon("Common Axe", 1, WeaponType.AXES, 2);

// create a new Armor with the following state
Armor armor = new Armor("Common Plate Chest", 1,Slot.BODY, ArmorType.PLATE, new Heroattribute(1, 0, 0));

// equip the warrior with the weapon and the armor
warrior.equip(weapon);
warrior.equip(armor);

// check the warrior's damage
System.out.println(warrior.damage()); // 2.05

// check the warrior's total attributes
System.out.println(warrior.totalAttributes()); // Heroattribute{strength=6, dexterity=2, intelligence=1}

// check the warrior's display
System.out.println(warrior.display());
/*
Name: John
Class: Warrior
Level: 1
Total strength: 6
Total dexterity: 2
Total intelligence: 1
Damage: 2.05
*/
```

## Testing

The code includes JUnit tests to ensure that the methods work as expected.

To run the tests, use the command:

`mvn test`

This will run all the tests and show the results in the console.

## CI/CD

This project includes a .gitlab-ci.yml file that can be used to set up a CI/CD pipeline on GitLab. The pipeline consists of the following stages:

- **build**: runs the Maven build and test phase
- **deploy**: deploys the application to a specified environment

By configuring the pipeline in GitLab, every time a change is pushed to the repository, the pipeline will automatically run and deploy the application if all stages pass successfully. This allows for continuous integration and continuous delivery of the application.

## Conclusion

This project demonstrates an implementation of a role-playing game that follows the Liskov Substitution Principle and includes a CI/CD pipeline using GitLab. The application includes various classes and functionality, such as hero classes, equipment, and items, and can be easily extended and modified to add new features. The included JUnit test cases ensure the correct functionality of the application.
