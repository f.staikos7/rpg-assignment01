package org.Assignment01.RPG;

import org.Assignment01.RPG.classes.Mage;
import org.Assignment01.RPG.classes.Ranger;
import org.Assignment01.RPG.enums.ArmorType;
import org.Assignment01.RPG.equipment.Item;
import org.Assignment01.RPG.classes.Rogue;
import org.Assignment01.RPG.classes.Warrior;
import org.Assignment01.RPG.enums.Slot;
import org.Assignment01.RPG.enums.WeaponType;
import org.Assignment01.RPG.equipment.Armor;
import org.Assignment01.RPG.equipment.Weapon;

import java.util.*;
abstract public class Hero {
    protected String name;
    protected int level;
    protected Heroattribute levelAttributes;
    protected List<WeaponType> validWeaponTypes;
    protected List<ArmorType> validArmorTypes;
    protected Map<Slot, Item> equipment;

    //Hero constructor
    public Hero(String name) {
        this.name = name;
        this.level = 1;
        this.levelAttributes = new Heroattribute(0, 0, 0);
        this.equipment = new HashMap<>();
        this.equipment.put(Slot.WEAPON, null);
        this.equipment.put(Slot.HEAD, null);
        this.equipment.put(Slot.BODY, null);
        this.equipment.put(Slot.LEGS, null);
        this.validWeaponTypes = new ArrayList<>();
        this.validArmorTypes = new ArrayList<>();
    }

    //level up function that adds attributes when leveling up
    public void levelUp() {
        this.level++; //adds +1 to the level counter
        Heroattribute gain = this.getLevelAttributeGain(); 
        this.levelAttributes = this.levelAttributes.add(gain);//adds the level up attributes to the previous attributes
    }

    //equip function that checks if weapon and armor are valid for the selected class and if the required level is met and then equips the item
    public void equip(Item item) throws InvalidItemException {
        if (item instanceof Weapon) {
            if (!validWeaponTypes.contains(((Weapon) item).getWeaponType())) {
                throw new InvalidWeaponException("This hero cannot equip this weapon type.");
            }
        } else {
            if (!validArmorTypes.contains(((Armor) item).getArmorType())) {
                throw new InvalidArmorException("This hero cannot equip this armor type.");
            }
        }
        if (item.getRequiredLevel() > this.level) {
            throw new InvalidItemException("This hero is not high enough level to equip this item.");
        }
        this.equipment.put(item.getSlot(), item);
    }

    //damage function that calculates the damage that is done for each class
    public double damage() {
        Heroattribute totalAttributes = this.totalAttributes();
        double weaponDamage=1;
        double damagingAttribute = 0;
        Item weapon = this.equipment.get(Slot.WEAPON);//gets weapon
        if(weapon != null){ //checks if weapon is equipped
            weaponDamage = ((Weapon) weapon).getWeaponDamage(); //it is supposed to get damage from weapon
        }
        // checks the class and adds its appropriate attribute to the damage
        if (this instanceof Warrior) {
            damagingAttribute = totalAttributes.getStrength();
        } else if (this instanceof Mage) {
            damagingAttribute = totalAttributes.getIntelligence();
        } else if (this instanceof Ranger || this instanceof Rogue) {
            damagingAttribute = totalAttributes.getDexterity();
        }

        return (weaponDamage * (1 + damagingAttribute / 100));//calculates damage
    }

    //total attributes function that adds them
    public Heroattribute totalAttributes() {
        Heroattribute total = new Heroattribute(this.levelAttributes.getStrength(), this.levelAttributes.getDexterity(), this.levelAttributes.getIntelligence());//gets hero attributes
        for (Map.Entry<Slot, Item> entry : this.equipment.entrySet()) {
            if (entry.getKey() != Slot.WEAPON) {//checks if item is not a weapon
                Armor armor = (Armor) entry.getValue();
                if(armor != null){//checks if armor equipped
                    Heroattribute armorGain=armor.getArmorAttribute();//gets armor attributes
                    total = total.add(armorGain);//adds them to the total attributes
                }
            }
        }
        return total;
    }

    //display function to show our heroes
    public String display() {
        StringBuilder sb = new StringBuilder();
        sb.append("Name: ").append(this.name).append("\n");
        sb.append("Class: ").append(this.getClass().getSimpleName()).append("\n");
        sb.append("Level: ").append(this.level).append("\n");
        sb.append("Total strength: ").append(this.totalAttributes().getStrength()).append("\n");
        sb.append("Total dexterity: ").append(this.totalAttributes().getDexterity()).append("\n");
        sb.append("Total intelligence: ").append(this.totalAttributes().getIntelligence()).append("\n");
        sb.append("Damage: ").append(this.damage()).append("\n");
        return sb.toString();
    }

    protected abstract Heroattribute getLevelAttributeGain();
}

//exceptions
class InvalidItemException extends Exception {
    public InvalidItemException(String message) {
        super(message);
    }
}
class InvalidWeaponException extends InvalidItemException {
    public InvalidWeaponException(String message) {
        super(message);
    }
}

class InvalidArmorException extends InvalidItemException {
    public InvalidArmorException(String message) {
        super(message);
    }
}
