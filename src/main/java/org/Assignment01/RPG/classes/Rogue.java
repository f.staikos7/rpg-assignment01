package org.Assignment01.RPG.classes;

import org.Assignment01.RPG.Hero;
import org.Assignment01.RPG.Heroattribute;
import org.Assignment01.RPG.enums.ArmorType;
import org.Assignment01.RPG.enums.WeaponType;
import org.Assignment01.RPG.equipment.Item;

public class Rogue extends Hero {

    //constructor
    public Rogue(String name) {
        super(name);
        this.levelAttributes = new Heroattribute(2, 6, 1);

    }

    //valid items
    @Override
    public void equip(Item item) {
        // implementation for equipping items specific to Rogues
        this.validWeaponTypes.add(WeaponType.DAGGERS);
        this.validWeaponTypes.add(WeaponType.SWORDS);
        this.validArmorTypes.add(ArmorType.LEATHER);
        this.validArmorTypes.add(ArmorType.MAIL);
    }

    //attribute gains
    @Override
    protected Heroattribute getLevelAttributeGain() {
        return new Heroattribute(1, 4, 1);
    }
    public int getLevel() {
        return level;
    }
}
