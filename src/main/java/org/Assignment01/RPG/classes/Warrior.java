package org.Assignment01.RPG.classes;

import org.Assignment01.RPG.Hero;
import org.Assignment01.RPG.Heroattribute;
import org.Assignment01.RPG.enums.ArmorType;
import org.Assignment01.RPG.enums.WeaponType;
import org.Assignment01.RPG.equipment.Item;

public class Warrior extends Hero {

    //class constructor
    public Warrior(String name) {
        super(name);
        this.levelAttributes = new Heroattribute(5, 2, 1);
    }

    //valid items
    @Override
    public void equip(Item item) {
        // implementation for equipping items specific to Warriors
        this.validWeaponTypes.add(WeaponType.AXES);
        this.validWeaponTypes.add(WeaponType.HAMMERS);
        this.validWeaponTypes.add(WeaponType.SWORDS);
        this.validArmorTypes.add(ArmorType.MAIL);
        this.validArmorTypes.add(ArmorType.PLATE);
    }

    //gain attributes
    @Override
    protected Heroattribute getLevelAttributeGain() {

        return new Heroattribute(3, 2, 1);
    }


    public int getLevel() {
        return level;
    }
}
