package org.Assignment01.RPG.classes;

import org.Assignment01.RPG.Hero;
import org.Assignment01.RPG.Heroattribute;
import org.Assignment01.RPG.equipment.Item;
import org.Assignment01.RPG.enums.ArmorType;
import org.Assignment01.RPG.enums.WeaponType;

public class Mage extends Hero {

    public Mage(String name) {
        super(name);
        this.levelAttributes = new Heroattribute(1, 1, 8);

    }

    //valid items
    @Override
    public void equip(Item item) {
        // implementation for equipping items specific to Mages
        this.validWeaponTypes.add(WeaponType.STAFFS);
        this.validWeaponTypes.add(WeaponType.WANDS);
        this.validArmorTypes.add(ArmorType.CLOTH);
    }

    //attributes gain
    @Override
    protected Heroattribute getLevelAttributeGain() {
        return new Heroattribute(1, 1, 5);
    }
    public int getLevel() {
        return level;
    }
}
