package org.Assignment01.RPG.classes;

import org.Assignment01.RPG.Hero;
import org.Assignment01.RPG.Heroattribute;
import org.Assignment01.RPG.equipment.Item;
import org.Assignment01.RPG.enums.ArmorType;
import org.Assignment01.RPG.enums.WeaponType;

public class Ranger extends Hero {

    //constructor
    public Ranger(String name) {
        super(name);
        this.levelAttributes = new Heroattribute(1, 7, 1);
    }

    //valid items
    @Override
    public void equip(Item item) {
        // implementation for equipping items specific to Rangers
        this.validWeaponTypes.add(WeaponType.BOWS);
        this.validArmorTypes.add(ArmorType.LEATHER);
        this.validArmorTypes.add(ArmorType.MAIL);
    }

    //attributes gain
    @Override
    protected Heroattribute getLevelAttributeGain() {
        return new Heroattribute(1, 5, 1);
    }
    public int getLevel() {
        return level;
    }
}
