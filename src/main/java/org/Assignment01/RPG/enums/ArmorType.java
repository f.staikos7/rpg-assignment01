package org.Assignment01.RPG.enums;

public enum ArmorType {
    CLOTH, LEATHER, MAIL, PLATE
}
