package org.Assignment01.RPG.enums;

public enum WeaponType {
    AXES, BOWS, DAGGERS, HAMMERS, STAFFS, SWORDS, WANDS
}
