package org.Assignment01.RPG;

import org.Assignment01.RPG.classes.Mage;
import org.Assignment01.RPG.classes.Ranger;
import org.Assignment01.RPG.classes.Rogue;
import org.Assignment01.RPG.classes.Warrior;
import org.Assignment01.RPG.enums.ArmorType;
import org.Assignment01.RPG.enums.Slot;
import org.Assignment01.RPG.enums.WeaponType;
import org.Assignment01.RPG.equipment.Armor;
import org.Assignment01.RPG.equipment.Weapon;

public class Main {
    public static void main(String[] args) throws InvalidItemException {
        Hero mage = new Mage("Merlin");
        mage.levelUp();
        Armor clothRobe = new Armor("Cloth Robe", 1, Slot.BODY, ArmorType.CLOTH, new Heroattribute(1, 2, 3));
        mage.equip(clothRobe);
        Weapon staff = new Weapon("Staff of Power", 1, WeaponType.STAFFS, 10);
        mage.equip(staff);
        System.out.println(mage.display());

        Hero ranger = new Ranger("Robin");
        ranger.levelUp();
        Armor leatherVest = new Armor("Leather Vest", 1,Slot.BODY, ArmorType.LEATHER, new Heroattribute(1, 2, 0));
        ranger.equip(leatherVest);
        Weapon bow = new Weapon("Longbow", 1, WeaponType.BOWS, 10);
        ranger.equip(bow);
        System.out.println(ranger.display());

        Hero rogue = new Rogue("Assassin");
        rogue.levelUp();
        Armor leatherPants = new Armor("Leather Pants", 1,Slot.LEGS, ArmorType.LEATHER, new Heroattribute(0, 2, 0));
        rogue.equip(leatherPants);
        Weapon dagger = new Weapon("Dagger", 1, WeaponType.DAGGERS, 10);
        rogue.equip(dagger);
        System.out.println(rogue.display());

        Hero warrior = new Warrior("Conan");
        warrior.levelUp();
        Armor plateChest = new Armor("Plate Chest", 1,Slot.BODY, ArmorType.PLATE, new Heroattribute(3, 0, 0));
        warrior.equip(plateChest);
        Weapon sword = new Weapon("Sword", 1, WeaponType.SWORDS, 10);
        warrior.equip(sword);
        System.out.println(warrior.display());
    }

}