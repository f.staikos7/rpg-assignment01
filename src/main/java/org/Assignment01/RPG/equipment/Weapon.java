package org.Assignment01.RPG.equipment;

import org.Assignment01.RPG.enums.WeaponType;
import org.Assignment01.RPG.enums.Slot;

public class Weapon extends Item {
    private WeaponType weaponType;
    private int weaponDamage;

    //weapons constructor
    public Weapon(String name, int requiredLevel, WeaponType weaponType, int weaponDamage) {
        super(name, 1, Slot.WEAPON);
        this.weaponType = weaponType;
        this.weaponDamage = weaponDamage;
    }

    //getters
    public WeaponType getWeaponType() {
        return weaponType;
    }

    public int getWeaponDamage() {
        return weaponDamage;
    }
}
