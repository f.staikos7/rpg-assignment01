package org.Assignment01.RPG.equipment;

import org.Assignment01.RPG.Heroattribute;
import org.Assignment01.RPG.enums.ArmorType;
import org.Assignment01.RPG.enums.Slot;

public class Armor extends Item {
    private ArmorType armorType;
    private Heroattribute armorAttribute;

    //armor constructor
    public Armor(String name, int requiredLevel, Slot slot, ArmorType armorType, Heroattribute armorAttribute) {
        super(name, requiredLevel, slot);
        this.armorType = armorType;
        this.armorAttribute = armorAttribute;
    }

    //getters
    public ArmorType getArmorType() {
        return armorType;
    }

    public Heroattribute getArmorAttribute() {
        return armorAttribute;
    }
}
