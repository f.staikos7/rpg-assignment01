package org.Assignment01.RPG.equipment;

import org.Assignment01.RPG.enums.Slot;

abstract public class Item {
    protected String name;
    protected int requiredLevel;
    protected Slot slot;

    //items constructor
    public Item(String name, int requiredLevel, Slot slot) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
    }

    //getters
    public String getName() {
        return name;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public Slot getSlot() {
        return slot;
    }
}

