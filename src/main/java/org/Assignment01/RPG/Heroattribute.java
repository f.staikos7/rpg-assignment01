package org.Assignment01.RPG;

public class Heroattribute {
    private int strength;
    private int dexterity;
    private int intelligence;


    //Heroattribute constructor
    public Heroattribute(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    //function to add attributes
    public Heroattribute add(Heroattribute other) {
        int strength = this.strength + other.getStrength();
        int dexterity = this.dexterity + other.getDexterity();
        int intelligence = this.intelligence + other.getIntelligence();
        return new Heroattribute(strength, dexterity, intelligence);
    }


}