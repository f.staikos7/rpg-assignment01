package org.Assignment01.RPG;

import org.Assignment01.RPG.classes.Warrior;
import org.Assignment01.RPG.enums.ArmorType;
import org.Assignment01.RPG.enums.Slot;
import org.Assignment01.RPG.enums.WeaponType;
import org.Assignment01.RPG.equipment.Armor;
import org.Assignment01.RPG.equipment.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HeroTest {
    @Test
    public void testLevelUp() {
        Warrior warrior = new Warrior("Conan");
        assertEquals(1, warrior.getLevel());
        warrior.levelUp();
        assertEquals(2, warrior.getLevel());
        assertEquals(8, warrior.totalAttributes().getStrength());
        assertEquals(4, warrior.totalAttributes().getDexterity());
        assertEquals(2, warrior.totalAttributes().getIntelligence());
    }

    @Test
    public void testEquip() {
        Warrior warrior = new Warrior("Conan");
        Weapon sword = new Weapon("Sword", 1, WeaponType.SWORDS, 10);
        Armor plateChest = new Armor("Plate Chest", 1, Slot.BODY, ArmorType.PLATE, new Heroattribute(3, 0, 0));
        warrior.equip(sword);
        warrior.equip(plateChest);
    }

    @Test
    public void testInvalidEquip() {
        Warrior warrior = new Warrior("Conan");
        Weapon staff = new Weapon("Staff",1, WeaponType.STAFFS, 10);
        Armor clothChest = new Armor("Cloth Chest", 1,Slot.BODY, ArmorType.CLOTH, new Heroattribute(3, 0, 0));
        warrior.equip(staff);
        fail("Should have thrown an exception");
        warrior.equip(clothChest);
        fail("Should have thrown an exception");
    }
    @Test
    public void testDamage() {
        Warrior warrior = new Warrior("Conan");
        assertEquals(1.08, warrior.damage());
        Weapon sword = new Weapon("Sword", 1, WeaponType.SWORDS, 10);
        Armor plateChest = new Armor("Plate Chest", 1,Slot.BODY, ArmorType.PLATE, new Heroattribute(3, 0, 0));
        warrior.equip(sword);
        warrior.equip(plateChest);
        assertEquals(11.11, warrior.damage());
    }

    @Test
    public void testTotalAttributes() {
        Warrior warrior = new Warrior("Conan");
        Weapon sword = new Weapon("Sword", 1, WeaponType.SWORDS, 10);
        Armor plateChest = new Armor("Plate Chest", 1,Slot.BODY, ArmorType.PLATE, new Heroattribute(3, 0, 0));
        warrior.equip(sword);
        warrior.equip(plateChest);
        Heroattribute total = warrior.totalAttributes();
        assertEquals(11, total.getStrength());
        assertEquals(4, total.getDexterity());
        assertEquals(2, total.getIntelligence());
    }

}
